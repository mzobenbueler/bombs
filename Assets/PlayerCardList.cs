﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerCard List", menuName = "BDSystem/PlayerCard List")]
public class PlayerCardList : ScriptableObject {

    public PlayerCard[] playerCardList = new PlayerCard[0];

}
