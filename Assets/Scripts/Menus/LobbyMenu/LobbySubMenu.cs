﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using InControl;

public class LobbySubMenu : MonoBehaviour {

    public LobbyMenuManager lobbyManager;

    public enum SubLobbyState {PressAny, CharSelect, ReadyFinal, Start}
    public SubLobbyState state;
    SubLobbyState nextState;
    SubLobbyState previousState;

    public TMP_Text instructionText;
    public TMP_Text controllerText;
    public TMP_Text playerNumberText;
    public Image characterImage;

    public int lobbyNumber;
    public Color lobbyColor;

    public LobbyController lobbyController;
    public PlayerConfiguration playerConfiguration;

    public int colorIndex;
    public int characterIndex;

    void Start()
    {
        state = SubLobbyState.PressAny;
        InitializeSelectedState(state);
        playerNumberText.text = lobbyNumber.ToString();
    }

    public void Initialize(LobbyMenuManager lobbymanager, int lobbynumber){
        lobbyManager = lobbymanager;
        lobbyNumber = lobbynumber;
    }

    public void AssignControllerToSubLobby(InputDevice device){
        lobbyController.controller = device;
        SetNextState();
    }

    void ErasePlayeConfiguration(){
        playerConfiguration = null;
    }

    void SavePlayerConfiguration(){
        playerConfiguration = new PlayerConfiguration(lobbyNumber, lobbyController.controller, lobbyColor);

        //Mettre un personnage par defaut tant qu'on a pas de facon de selectionner
        playerConfiguration.playerCharacterCard = GameManager.gameManager.playersManager.availableCharacters.characterCardList[0];
    }

    public PlayerConfiguration GetPlayerConfigurationFromLobbySubMenu(){
        return playerConfiguration;
    }

    void Update(){

        switch(state){
        case SubLobbyState.PressAny:
            if(lobbyController.button2){
                lobbyManager.CheckForReturningToStartMenu();
                return;
            }
            break;

        case SubLobbyState.CharSelect:
            if(lobbyController.left){

            }
            if(lobbyController.right){

            }
            break;

        case SubLobbyState.ReadyFinal:
            break;

        case SubLobbyState.Start:
            if(lobbyController.button1){
                lobbyManager.CheckForGoingToNewGameMenu();
                return;
            }
            break;
        }

        if(lobbyController.button1){
            SetNextState();
            return;
        }

        if(lobbyController.button2){
            SetPreviousState();     
            return;
        }
    }    

    void GetNextColor(int dir){
        
    }

    void GetNextCharacter(int dir){

    }

    void SetNextState(){
        state = nextState;
        InitializeSelectedState(state);        
    }

    void SetPreviousState(){
        state = previousState;
        InitializeSelectedState(state); 
    }

    void InitializeSelectedState(SubLobbyState selectedState){
        switch(state){
            case SubLobbyState.PressAny:
                instructionText.text = "Press any button to join";
                characterImage.enabled = false;
                controllerText.text = "";

                nextState = SubLobbyState.CharSelect;
                previousState = SubLobbyState.PressAny;
                break;

            case SubLobbyState.CharSelect:
                ErasePlayeConfiguration();
                instructionText.text = "Select your Color";
                characterImage.enabled = true;
                controllerText.text = lobbyController.controller.Name;

                nextState = SubLobbyState.ReadyFinal;
                previousState = SubLobbyState.PressAny;
                break;

            case SubLobbyState.ReadyFinal:
                SavePlayerConfiguration();
                instructionText.text = "Ready !";

                nextState = SubLobbyState.Start;
                previousState = SubLobbyState.CharSelect;
            break;

            case SubLobbyState.Start:
            break;
        }
    }  
}
