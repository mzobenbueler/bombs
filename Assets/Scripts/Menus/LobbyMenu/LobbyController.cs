﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class LobbyController : MonoBehaviour {

    public LobbySubMenu menu;
    public InputDevice controller;

    public float actionButtonRate;
    public float directionButtonRate;

    public bool button1;
    public bool button1Ready;
    public bool button2;
    public bool button2Ready;
    public bool restartButton;

    public bool up;
    public bool down;
    public bool left;
    public bool right;
    public bool directionReady;

    // Use this for initialization
    void Start () {
        button1Ready = true;
        button2Ready = true;
        directionReady = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (controller != null)
        {
            UseConnectedMenu();
        }
    }


    //UseConnectedCanvas
    void UseConnectedMenu()
    {
        ResetButtons();
        //Se deplacer dans un Canvas connecte
        if (controller.Action1.WasPressed && button1Ready)
        {
            button1 = true;
            StartCoroutine("Button1Wait");
        }

        if (controller.Action2.WasPressed && button2Ready)
        {
            button2 = true;
            StartCoroutine("Button2Wait");
        }

        if (controller.Direction.WasPressed && directionReady)
        {
            TwoAxisInputControl zzz = controller.Direction;

            //Conversion de l'angle du stick en direction Haut Droite Bas Gauche
            int angle = Mathf.RoundToInt(zzz.Angle);

            if (angle > 315 || angle <= 45)
            {
                up = true;
            }
            else if (angle > 45 && angle <= 135)
            {
                left = true;
            }
            else if (angle > 135 && angle <= 215)
            {
                down = true;
            }
            else if (angle > 215 && angle <= 315)
            {
                right = true;
            }
            StartCoroutine("DirectionWait");
        }

    }


    IEnumerator Button1Wait()
    {
        button1Ready = false;

        yield return new WaitForSeconds(actionButtonRate);
        button1Ready = true;
    }

    IEnumerator Button2Wait()
    {
        button2Ready = false;

        yield return new WaitForSeconds(actionButtonRate);
        button2Ready = true;
    }

    IEnumerator DirectionWait()
    {
        directionReady = false;

        yield return new WaitForSeconds(directionButtonRate);
        directionReady = true;
    }

	void ResetButtons(){
		
		if (button1)
			button1 = false;

		if (button2)
			button2 = false;

		if (up)
			up = false;

		if (down)
			down = false;

		if (left) 
			left = false;			

		if (right)
			right = false;

	}
}
