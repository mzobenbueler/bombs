﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyMenu : Menu<LobbyMenu> {

    public LobbySubMenu subMenuPrefab;
    public LobbySubMenu[] subMenus;
    public LobbyMenuManager lobbyManager;

    public LobbyMenuManager mananger;

    public int intPlayers;

    
       
    public static void Show(bool multiplayer){

        Open();

        Instance.mananger = new LobbyMenuManager(Instance);
        Instance.mananger.ShowLobby(multiplayer);
    } 

    public override void OnBackPressed(){
		//MenuManager.Instance.CloseMenu(this);
	}	

    public void ReturnToStartMenu(){
    	MenuManager.Instance.CloseMenu(this);
    }

    public void GoToGameModeMenu(){
        GameModeMenu.Show();
    }

}
