﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using InControl;


public class LobbyMenuManager {
    
    LobbyMenu lobbyMenu;
    bool multiplayerLobby;
    LobbySubMenu[] subMenus;

    List<InputDevice> nonRegisteredDevices = new List<InputDevice>();
    List<InputDevice> registeredDevices = new List<InputDevice>();

    public LobbyMenuManager(LobbyMenu menu){
        lobbyMenu = menu;
    }

    void FixedUpdate() //Ne va pas marcher car ce script nest pas Monobehavior
    {
        CheckForNewPlayer();
    }

#region Initialisation
    public void ShowLobby(bool multi){

        multiplayerLobby = multi;

        registeredDevices = new List<InputDevice>();
        nonRegisteredDevices = new List<InputDevice>(InputManager.Devices);

        if(multiplayerLobby){
            ShowMultiplayerLobby();
        } else {
            ShowSoloplayerLobby();
        }

    }

    void ShowSoloplayerLobby(){

    }

    void ShowMultiplayerLobby(){

        InitializeSubLobbies();

        RegisterFirstPlayerDevice();

    }

    void InitializeSubLobbies(){

        LobbySubMenu lsmPrefab = UnityEngine.Resources.Load<LobbySubMenu>("LobbySubMenu");

        subMenus = new LobbySubMenu[4];

        for (int i = 0; i < 4; i++)
        {
            subMenus[i] = GameObject.Instantiate(lsmPrefab, lobbyMenu.transform);
            subMenus[i].Initialize(this, i);
        }

        SetLobbyDimensionForMultiplayer();        
    }

    void SetLobbyDimensionForMultiplayer()
    {
        for (int i = 0; i < subMenus.Length; i++)
        {
            RectTransform rect = subMenus[i].GetComponent<RectTransform>();
            switch (i)
            {
                case 0:
                    rect.position = new Vector2(0 + Screen.width / 4, Screen.height / 4 + Screen.height / 2);
                    break;
                case 1:
                    rect.position = new Vector2(Screen.width / 4 + Screen.width / 2, Screen.height / 4 + Screen.height / 2);
                    break;
                case 2:
                    rect.position = new Vector2(Screen.width / 4, Screen.height / 4);
                    break;
                case 3:
                    rect.position = new Vector2(Screen.width / 4 + Screen.width / 2, Screen.height / 4);
                    break;
            }
            rect.localScale = Vector3.one * 0.5f;
        }
    }
#endregion

#region Add New Player
    void CheckForNewPlayer()
    {
        if (InputManager.ActiveDevice.Action1.WasPressed)
        {            
            for (int i = 0; i < nonRegisteredDevices.Count; i ++){

                InputDevice tempDevice = nonRegisteredDevices[i];
                if(InputManager.ActiveDevice == tempDevice)
                {
                    RegisterAndSetupLastUsedDevice(tempDevice);
                }
            }
        }
    }

    void RegisterFirstPlayerDevice(){
        RegisterAndSetupLastUsedDevice(InputManager.ActiveDevice);
    }
         
    void RegisterAndSetupLastUsedDevice(InputDevice device)
    {
        nonRegisteredDevices.Remove(device);

        registeredDevices.Add(device);

        subMenus[registeredDevices.Count - 1].AssignControllerToSubLobby(device);
    }
#endregion

#region Color Changing
    public List<Color> GetAvailableCharacterColorList()
    {
        return GameManager.gameManager.SOList.characterColorsList.colorsList;        
    }








    
#endregion

#region Change Menu
    public void CheckForReturningToStartMenu(){

        bool readyToReturnToStartMenu = true;
        for(int i = 0; i < subMenus.Length; i ++){
            if(subMenus[i].state != LobbySubMenu.SubLobbyState.PressAny){
                readyToReturnToStartMenu = false; 
            }
        }

        if(readyToReturnToStartMenu){
            lobbyMenu.ReturnToStartMenu();
        }        
    }

    public void CheckForGoingToNewGameMenu(){
        
        bool readyToGoToNextMenu = true;
        int detectedPlayersNumber = 0;
        for(int i = 0; i < subMenus.Length; i ++){

            if(subMenus[i].state != LobbySubMenu.SubLobbyState.Start
            && subMenus[i].state != LobbySubMenu.SubLobbyState.PressAny){
                readyToGoToNextMenu = false;
            } else {
                detectedPlayersNumber += 1;
            }
        }

        if(readyToGoToNextMenu){
            SetPlayerConfigurationIntoPlayersManager();
            lobbyMenu.GoToGameModeMenu();
        }
    }    

    void SetPlayerConfigurationIntoPlayersManager(){

        List<PlayerConfiguration> pcs = new List<PlayerConfiguration>();

        int tempPlayerNumber = 0;
        foreach(LobbySubMenu submenu in subMenus){

            if(submenu.state == LobbySubMenu.SubLobbyState.Start || submenu.state == LobbySubMenu.SubLobbyState.ReadyFinal ){
                pcs.Add(submenu.GetPlayerConfigurationFromLobbySubMenu());
                pcs[tempPlayerNumber].playerNumber = tempPlayerNumber;
                tempPlayerNumber ++;
            }
        }        
        GameManager.gameManager.playersManager.SetNewPlayersConfigurationIntoPlayersManager(pcs);
    }
#endregion

}
