﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LevelObjectMenu : MonoBehaviour
{

    public TMP_Text levelNumberText;
    public TMP_Text levelTitleText;
    public TMP_Text levelDescriptionText;
    public Image levelImage;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
