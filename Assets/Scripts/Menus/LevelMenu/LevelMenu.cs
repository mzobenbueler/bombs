﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LevelMenu : Menu<LevelMenu> {

	public LevelObjectMenu loPrefab;
	public LevelObjectMenu[] lObjects;

	public LevelData[] levelList;
	public LevelData[] availableLevels;
	int intSelectedLevelData;

	public float decalageHorizontal;

	public static void Show(){
		Open();
	}

	private void OnEnable(){
		
		levelList = GameManager.gameManager.SOList.levelsList.leveldatas;

		InstantiateLevelObjects();

		SetLevelToSelectedState(intSelectedLevelData);
	}

	void InstantiateLevelObjects(){
		lObjects = new LevelObjectMenu[levelList.Length];

		for(int i = 0; i < lObjects.Length; i ++){

			LevelObjectMenu lo = Instantiate(loPrefab);

			lo.transform.SetParent(this.transform);

			lo.transform.position += Vector3.right * (i * decalageHorizontal - 200);

			lo.levelTitleText.text = levelList[i].mapName;
			lo.levelDescriptionText.text = levelList[i].levelDescription;

			int levelNo = i + 1;
			lo.levelNumberText.text = levelNo.ToString();			
			
			if(levelList[i].imageLevel != null){
				lo.levelImage.sprite = levelList[i].imageLevel;
			}

			lObjects[i] = lo;
		}
	}

	void SetLevelToSelectedState(int newLevelNumber){

		LevelObjectMenu loOld = lObjects[intSelectedLevelData];
		loOld.levelTitleText.color = Color.black;

		LevelObjectMenu loNew = lObjects[newLevelNumber];
		loNew.levelTitleText.color = Color.red;

		intSelectedLevelData = newLevelNumber;
	}

	public override void OnLeftPressed(){
		int newLevelNumber = intSelectedLevelData - 1;

		if(newLevelNumber < 0){
			newLevelNumber = levelList.Length - 1;
		}

		SetLevelToSelectedState(newLevelNumber);
	}

	public override void OnRightPressed(){
		int newLevelNumber = intSelectedLevelData + 1;

		if(newLevelNumber > levelList.Length - 1){
			newLevelNumber = 0;
		}

		SetLevelToSelectedState(newLevelNumber);
	}

	public override void OnValidationPressed(){
        StartSelectedGame();
    }

	void StartSelectedGame(){

		GameManager.gameManager.SetNewLevelData(levelList[intSelectedLevelData]);

		GameManager.gameManager.StartLevel ();
	}
}
