﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GameModeObjectMenu : MonoBehaviour
{
    public TMP_Text gameModeTitleText;
    public TMP_Text gameModeDescriptionText;
    public Image gameModeImage;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
