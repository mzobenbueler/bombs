﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GameModeMenu : Menu<GameModeMenu>
{    
    public GameModeObjectMenu gmoPrefab;
    public GameModeObjectMenu[] gmObjects;

	public GameModeData[] gameModeList;
	public GameModeData[] availableGameModes;
	public GameModeData selectedGameModeData;
	int intSelectedGameMode;

    public float decalageHorizontal;

    public static void Show(){
        Open();        
    }

    private void OnEnable(){
        
        gameModeList = GameManager.gameManager.SOList.gameModesList.gameModeDatas;
        InstantiateGameModeObjects();

        SetGameModeToSelectedState(intSelectedGameMode);
    }

    // GameModeData[] LoadGameModeList (){
	// 	GameModeListData dataGameModeList = (GameModeListData)AssetDatabase.LoadAssetAtPath ( UsefulPath.listGameModeData, typeof(GameModeListData));
	// 	return dataGameModeList.gameModeDatas;  
    // }

    void InstantiateGameModeObjects(){

        gmObjects = new GameModeObjectMenu[gameModeList.Length];

        for (int i = 0; i < gmObjects.Length; i ++){

            GameModeObjectMenu gmo = Instantiate(gmoPrefab);

            gmo.transform.SetParent(this.transform);

            gmo.transform.position += Vector3.right * (i * decalageHorizontal - 200);

            gmo.gameModeTitleText.text = gameModeList[i].gameModeName;
            gmo.gameModeDescriptionText.text = gameModeList[i].gameModeDescription;
            
            if(gameModeList[i].gameModeSprite != null){
                gmo.gameModeImage.sprite = gameModeList[i].gameModeSprite;
            }     
            gmObjects[i] = gmo;
        }
    }

    void SetGameModeToSelectedState(int newGameModeNumber){

        GameModeObjectMenu gmoOld = gmObjects[intSelectedGameMode];
        gmoOld.gameModeTitleText.color = Color.black;

        GameModeObjectMenu gmoNew = gmObjects[newGameModeNumber];
        gmoNew.gameModeTitleText.color = Color.red;

        intSelectedGameMode = newGameModeNumber;
    }
    
    public override void OnBackPressed(){
		MenuManager.Instance.CloseMenu(this);
	}	

    public override void OnLeftPressed(){
        int newGameModeNumber = intSelectedGameMode - 1;

        if(newGameModeNumber < 0){
            newGameModeNumber = gameModeList.Length - 1;
        }

        SetGameModeToSelectedState(newGameModeNumber);
	}	

    public override void OnRightPressed(){
        int newGameModeNumber = intSelectedGameMode + 1;

        if(newGameModeNumber > gameModeList.Length - 1){
            newGameModeNumber = 0;
        }

        SetGameModeToSelectedState(newGameModeNumber);
    }	

    public override void OnValidationPressed(){

        GameManager.gameManager.SetNewGameModeData( gameModeList[intSelectedGameMode]);

        LevelMenu.Show();
    }
}
