﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

[System.Serializable]
public class PlayersManager : MonoBehaviour {

	public static PlayersManager playersManager;

	//Initializer
	//CharactersInitialisation playersInit;

	public int playersNumber;
	public PlayableCharacter playerBase;

	//Pour la visibilite des controlleurs, a supprimer
	public string[] devicesList;

	//Tableau des devices, a supprimer
	public InputDevice[] activeDevices;

    [SerializeField]public List<PlayerConfiguration> playerConfigurations = new List<PlayerConfiguration>();

    //Initialise par CharactersInitialisation
    public Character[] inUseCharacters;

	//Character selection from the Editor
	public CharacterCardList availableCharacters; 
	[SerializeField]public string[] availableCharactersStr;
	[SerializeField]public int[] selectedCharactersNo;
	//Color selection from the Editor
	[SerializeField]public Color[] selectedColors;

	void Awake (){
		//Ne pas detruire le chargement en changeant de scene
		DontDestroyOnLoad (gameObject);

		//Empecher que le Manager se duplique
		if (playersManager == null) {
			playersManager = this;
		} else {
			Destroy (gameObject);
		}
	}

//Appel depuis LobbyManager
	public void SetNewPlayersConfigurationIntoPlayersManager(List<PlayerConfiguration> pc){

		Debug.Log("Manager Recieving information from Lobby : " + pc.Count );
		
		Debug.Log(pc[0].playerCharacterCard.name);
		playerConfigurations = pc;

		Debug.Log("Result : " + playerConfigurations.Count );
	}

	public List<PlayerConfiguration> SetPlayerConfigurationFromEditorInformation(){

		playerConfigurations = new List<PlayerConfiguration>();
		int totalPlayers = GameManager.gameManager.nbrJoueursTemp;
		int totalControllers = InControl.InputManager.Devices.Count;

		for(int i = 0; i < totalPlayers; i ++){
			AddPlayerConfiguration();
			PlayerConfiguration pConfig = playerConfigurations[i];
			SetPlayerNumberIntoPlayerConfiguration(pConfig, i);
			SetColorIntoPlayerConfiguration(pConfig, selectedColors[i]);
			SetCharacterCardIntoPlayerConfiguration(pConfig, GetCharacterCardFromIndex(i));			
			if(i < totalControllers){
				SetInputDeviceIntoPlayerConfiguration(playerConfigurations[i], InControl.InputManager.Devices[i]);
			} else {
				SetInputDeviceIntoPlayerConfiguration(playerConfigurations[i], InControl.InputManager.Devices[0]);
			}
		}
		return playerConfigurations;
	}

	void GeneratePlayerConfigurations(){

	}

	void DeletePlayerConfiguration(PlayerConfiguration pConfig){

	}	

	void AddPlayerConfiguration(){
		playerConfigurations.Add(new PlayerConfiguration());
	}

	void SetPlayerNumberIntoPlayerConfiguration(PlayerConfiguration pConfig, int playerNumber){
		pConfig.playerNumber = playerNumber;
	}

	void SetInputDeviceIntoPlayerConfiguration(PlayerConfiguration pConfig, InputDevice device){
		pConfig.playerDevice = device;
	}

	void SetColorIntoPlayerConfiguration(PlayerConfiguration pConfig, Color color){
		pConfig.playerColor = color;	
	}

	void SetCharacterCardIntoPlayerConfiguration(PlayerConfiguration pConfig, CharacterCard charCard){
		pConfig.playerCharacterCard = charCard;
	}

	CharacterCard GetCharacterCardFromIndex(int index){
		CharacterCard card = availableCharacters.characterCardList[index];
		return card;
	}

#region Initialisation for PlayersManagerEditor 

	//Called from PlayersManagerEditor to select characters from the editor
	public void InitializeDataForCustomEditor(){

		//Character Cards Data
		availableCharactersStr = availableCharacters.characterCardNames;
		selectedCharactersNo = new int[availableCharactersStr.Length];

	}


#endregion

#region Enable or Disable all characters
	public void EnableAllCharacters(){

	}

	public void DisableAllCharacters(){

	}
#endregion

}