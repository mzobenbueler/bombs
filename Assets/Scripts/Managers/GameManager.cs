﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Modes;
using ManagerEnums;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager gameManager;
	public GameManagerMode gameManagerMode;
	public IGameManager actualGameManagerMode;

	//Donnees dans le manager
	public LevelData selectedLevelData;
	public GameModeData selectedGameModeData;
	public GameMode selectedGameModePrefab;

	//LevelData newLeveData;
	int newIntPlayers;

	public PlayersManager playersManager;
	public UIManager uiManager;

	[SerializeField]public LaunchingFrom launchingFrom;
	[SerializeField]public int nbrJoueursTemp;

	//Reference to every scriptableObjects of the game
	public ScriptableObjectsList SOList;

	void Awake ()
	{
		//Ne pas detruire le chargement en changeant de scene
		DontDestroyOnLoad (gameObject);
		//Empecher que le Manager se duplique
		if (gameManager == null) {
			gameManager = this;
		} else {
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		
		playersManager = PlayersManager.playersManager;
		uiManager = UIManager.uiManager;

		playersManager.playersNumber = nbrJoueursTemp;

		BootingGame ();
	}
	
	// Update is called once per frame
	void Update () {
		actualGameManagerMode.UpdateState ();
	}

	void ChangerMode(IGameManager newMode){
		actualGameManagerMode = newMode;
		actualGameManagerMode.Start ();
	}

	void BootingGame(){
		ChangerMode (new GMBootingGame ());
	}

	public void SceneLoading(string nameScene){
		SceneLoader loader = new SceneLoader (nameScene);
		loader.LoadScene ();
	}

	public void StartGame(){
		ChangerMode (new GMLaunchGameFromStart());
	}

	public void StartLevel(){
		ChangerMode (new GMLoadLevel ());
	}

    public void RestartLevel()
    {
        ChangerMode(new GMLoadLevel());
    }

    public void PlayLevel(){
		ChangerMode (new GMPlayingLevel ());
	}

	public void EndLevel(){
		ChangerMode(new GMEndedGame());
	}

    public void OtherState()
    {
        ChangerMode(new GMOtherState());
    }

	public void StartGMCoroutine(IEnumerator coroutineMethod){
		StartCoroutine (coroutineMethod);
	}

	public void SetNewGameModeData(GameModeData newgamemodedata){
		selectedGameModeData = newgamemodedata;

		selectedGameModePrefab = selectedGameModeData.gameMode;
		Debug.Log("GameModeData in GameManager: " + selectedGameModeData.gameModeName);
	}

	public void SetNewLevelData(LevelData newleveldata){
		selectedLevelData = newleveldata;
		Debug.Log("Selected Level Data in GameManager: " + selectedLevelData.mapName);
	}

	public GameModeData GetGameModeData(){
		return selectedGameModeData;
	}

	public LevelData GetLevelData(){
		return selectedLevelData;
	}

	public bool IsLaunchingFromLevelEditor(){
		return launchingFrom == LaunchingFrom.ThisLevel;
	}

}



public class GMBootingGame : IGameManager {

	private readonly GameManager gameManager;
	public GMBootingGame (){
		gameManager = GameManager.gameManager;
	}

	public void Start(){

		//Launched from Menu or Level
		switch (gameManager.launchingFrom) {
		case LaunchingFrom.StartMenu:
			gameManager.StartGame ();
			break;
		case LaunchingFrom.ThisLevel:
			gameManager.StartLevel ();
			break;
        case LaunchingFrom.Other:
            gameManager.OtherState();
            break;
		}
	}

	public void UpdateState(){
		
	}


}



// GMMAINMENU est en fait le START MENU
public class GMLaunchGameFromStart : IGameManager {

	private readonly GameManager gameManager;
	private SceneLoader sceneLoader;

	public GMLaunchGameFromStart (){
		gameManager = GameManager.gameManager;
	}

	public void Start (){		
		gameManager.SceneLoading ("StartScene");
	}

	public void UpdateState(){

	}
}





public class GMLoadLevel : IGameManager {
	
	private readonly GameManager gameManager;
	SetupScene setupScene;

	public GMLoadLevel (){
		gameManager = GameManager.gameManager;
	}

	public void Start (){

		//Use Editor information instead of Lobby to configure players
		if(gameManager.IsLaunchingFromLevelEditor()){
			gameManager.playersManager.SetPlayerConfigurationFromEditorInformation();
		}

		setupScene = new SetupScene(gameManager);
		gameManager.StartGMCoroutine(setupScene.SceneLoadingCoroutine());
	}

	public void UpdateState(){

	}

	//Utiliser la Coroutine par le gameManager qui est MonoBehaviour
	void CoroutineExemple(){
		gameManager.StartGMCoroutine (CoroutineTest());
	}

	IEnumerator CoroutineTest(){		
		yield return new WaitForSeconds (1);
	}
}




public class GMPlayingLevel : IGameManager {
	
	private readonly GameManager gameManager;


	public GMPlayingLevel (){
		gameManager = GameManager.gameManager;
	}

	public void Start (){
		//Jouer le GAME MODE
		gameManager.selectedGameModePrefab.GameStart();
	}

	public void UpdateState(){

		//Jouer le GAME MODE
		gameManager.selectedGameModePrefab.UpdateMode();
	}
}




public class GMEndedGame : IGameManager {
	
	private readonly GameManager gameManager;
	private SceneLoader sceneLoader;
	public GMEndedGame (){
		gameManager = GameManager.gameManager;
	}

	public void Start (){
		gameManager.SceneLoading ("EndMenu");
	}

	public void UpdateState(){

	}

	public void ReturnToMainMenu(){
		gameManager.StartGame ();
	}
}

public class GMOtherState : IGameManager
{

    private readonly GameManager gameManager;
    private SceneLoader sceneLoader;
    public GMOtherState()
    {
        gameManager = GameManager.gameManager;
    }

    public void Start()
    {
    }

    public void UpdateState()
    {

    }

    public void ReturnToMainMenu()
    {
        gameManager.StartGame();
    }
}