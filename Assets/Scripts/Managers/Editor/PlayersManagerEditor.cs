﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using InControl;

[CustomEditor(typeof(PlayersManager))]
public class PlayersManagerEditor : Editor {

	private PlayersManager pManager;
	private GameManager manager;
	private GameManagerEditor managerEditor;

	private int playersNumber;

	public InputDevice[] devices;
	public string[] devicesList;
	public PlayerConfiguration[] pConfig;




	void OnEnable(){
		pManager = (PlayersManager)target;

		manager = (GameManager)FindObjectOfType (typeof(GameManager)) as GameManager;

		//Initialize the name array to select characters from the editor
		if(pManager.availableCharactersStr.Length == 0){
			pManager.InitializeDataForCustomEditor();
		} 

		EditorApplication.update += Update;
	}

	void OnDisable(){
		EditorApplication.update -= Update;
	}

	void Update(){

		if (!EditorApplication.isPlaying){
			CheckForPlayersNumberUpdate();
		}
	}

	void CheckForPlayersNumberUpdate(){
		
		if(pManager.playerConfigurations.Count != manager.nbrJoueursTemp){	
			pManager.playerConfigurations = new List<PlayerConfiguration>();

			for(int i = 0; i < manager.nbrJoueursTemp; i ++){
				pManager.playerConfigurations.Add(new PlayerConfiguration(i));
			}
		}

		if (pManager.selectedColors.Length != 4){
			pManager.selectedColors = new Color[4];
		}
	}


	public override void OnInspectorGUI(){
		ShowPlayersSettingFromEditor ();

		//IMPORTANT
		//CONSERVER LES MODIFICATIONS
		EditorUtility.SetDirty (pManager);
	}


	void ShowPlayersSettingFromEditor(){

		for (int i = 0; i < pManager.playerConfigurations.Count; i ++){

			//Player Number
			EditorGUILayout.LabelField ("Player Number : " + i);

			pManager.selectedColors[i] = EditorGUILayout.ColorField ("Player Color", pManager.selectedColors[i]);
			pManager.selectedColors[i].a = 1f;

			//Player Character (selection by name)
			pManager.selectedCharactersNo[i] = EditorGUILayout.Popup(pManager.selectedCharactersNo[i], pManager.availableCharactersStr);

			EditorGUILayout.Space ();		

		}
	}
}
