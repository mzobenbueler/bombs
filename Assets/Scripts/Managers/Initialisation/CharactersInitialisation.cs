﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CharactersInitialisation {

	private readonly PlayersManager playersManager;
	private SetupScene setupScene;
	public CharactersInitialisation (SetupScene setupSceneFrom){
		playersManager = PlayersManager.playersManager;
		setupScene = setupSceneFrom;
	}

    private LevelData lvlData;
    private Character[] charactersToLoad;
    private SpawnPoint[] spawnList;

    public void Initialization(){

        spawnList = GetSpawnPoints();

        InstantiateCharacters();

		//Fin de l'initialisation
		setupScene.PlayersInitializationFinished ();
	}

    SpawnPoint[] GetSpawnPoints()
    {
        SpawnPoint[] tempSpawnList = new SpawnPoint[0];
        tempSpawnList = Object.FindObjectsOfType(typeof(SpawnPoint)) as SpawnPoint[];
        return tempSpawnList;
    }

    void InstantiateCharacters(){

        int playerNumber = playersManager.playerConfigurations.Count;        
        Character[] toInstantiate = GetCharactersFromPlayersConfig();

        for(int i = 0; i < playerNumber; i ++){
            toInstantiate[i] = GameObject.Instantiate(toInstantiate[i], Vector3.zero, Quaternion.Euler(Vector3.zero));
            CreateIsoCharacterController(toInstantiate[i], i, playersManager.playerConfigurations[i]);
            toInstantiate[i].SetPositionAndRespawnPosition(spawnList[i].gameObject.transform.position);
            SetInformationFromPlayerCard(toInstantiate[i], i);
        }

        SetReadyToUseCharactersIntoPlayersManager(toInstantiate);
    }

    Character[] GetCharactersFromPlayersConfig(){
        Character[] selectedCharacter = new Character[playersManager.playerConfigurations.Count];

        for(int i = 0; i < selectedCharacter.Length; i ++){     
            selectedCharacter[i] = playersManager.playerConfigurations[i].playerCharacterCard.characterPrefab;
        }

        return selectedCharacter;
    }

    void CreateIsoCharacterController(Character character, int i, PlayerConfiguration pConfig){
        IsoCharacterController ic =  playersManager.gameObject.AddComponent(typeof(IsoCharacterController)) as IsoCharacterController;
        ic.device = pConfig.playerDevice; //Connecter le device venant du PlayerCOnfig au IsoCharacterController. Revient a brancher le device sur le character        
        ic.characterMovements = character.GetComponent<IsoCharacterMovements>();
        ic.weapon = character.GetComponentInChildren<Weapon>(); //Devrait etre IsoCharacterWeapon
    }

    void SetReadyToUseCharactersIntoPlayersManager(Character[] characterToUse){
        playersManager.inUseCharacters = characterToUse;
    }



    //Instantiate Characters prefab from previously made array
    void InstantiateLoadedCharactersANCIEN()
    {
        //Creer les 4 personnages d'origine
        Character[] characterToInstantiate = new Character[playersManager.playersNumber];

        for (int i = 0; i < playersManager.playersNumber; i++)
        {
            //Instantiation
            characterToInstantiate[i] = GameObject.Instantiate(charactersToLoad[i], Vector3.zero, Quaternion.Euler(Vector3.zero));

            //PlayerConfig[] playerConfigs = playersManager.playersConfig;



            //Mettre le controleur - A DEPLACER DANS CONTROLLER MANAGER
            //IsoCharacterController isoCharControler = playerConfigs[i].gameObject.AddComponent(typeof(IsoCharacterController)) as IsoCharacterController;
            //isoCharControler.device = playerConfigs[i].controller;
            //isoCharControler.characterMovements = characterToInstantiate[i].GetComponent<IsoCharacterMovements>();
            //isoCharControler.weapon = characterToInstantiate[i].GetComponentInChildren<Weapon>();
            

            //Placer les personnages sur le SpawnPoint
            characterToInstantiate[i].transform.position = spawnList[i].gameObject.transform.position;
            //characterToInstantiate[i].respawnPosition = characterToInstantiate[i].transform.position;
            SetInformationFromPlayerCard(characterToInstantiate[i], i);
        }

        playersManager.inUseCharacters = new Character[characterToInstantiate.Length];
        playersManager.inUseCharacters = characterToInstantiate;
    }
     
	void SetInformationFromPlayerCard(Character character, int i){

        //Anciennement, on allait chercher la PlayerCard. Maintenant, on va faire avec le PlayerConfig du PlayersManager
		SpriteRenderer renderer = character.GetComponentInChildren<SpriteRenderer> ();
		renderer.color = playersManager.playerConfigurations[i].playerColor;

        //Voir si la classe PLAYABLE CHARACTER est toujours valable
		PlayableCharacter playablePart = character.GetComponent<PlayableCharacter> ();
		playablePart.playerNumber = i;
		playablePart.playerColor = playersManager.playerConfigurations[i].playerColor;        
	}

}
