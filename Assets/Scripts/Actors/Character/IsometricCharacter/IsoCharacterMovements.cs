﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
//[RequireComponent(typeof(Animator))]
public class IsoCharacterMovements : Character {

	[SerializeField]
	//A METTRE DANS UNE AUTRE CLASSE PLUS TARD
	public int playerNumber;

    public CapsuleCollider capsule;
    public Animator animator;


    //Variables de deplacement ect
	//[SerializeField] float walkingSpeed = 0.5f;
	[SerializeField] float runningSpeedAct;
    public float runningSpeedMax;
	[SerializeField] float runningSpeedAcceleration = 1f;
	[SerializeField] float velocityMagnetudeMax = 7f;
	public float brakingSpeed;
    public float airMoveVelocityMagnetudeMax = 7f;

	public float jumpPower = 360;
	[Range(1f, 100f)][SerializeField] float gravityMultiplier = 2f;
	public float groundCheckDistance = 0.1f;
    public float distanceFromGround;

	public bool isGrounded;
    public bool isInTheAir;
    public bool isOnLedge;

    public bool isLifted;
	//Vector3 groundNormal;

	public float wallCheckDistance;
	public bool isOnWall;
	Vector3 wallNormal;
	Vector3 wallDecalage;

    public float ledgeCheckDistance = 1f;
    public float ledgeClimbingSpeed = 1f;

    public bool canLedgeGrab;
    public float canClimbTime;

    public bool jumpButtonReleased;
    public Vector3 inputMoveDirectionLatest;
    public float inputMoveDistance;

    public Vector3 ledgeGrabPosition;
    public Vector3 ledgeGrabDestination;
    public GameObject actualLedgeGrabObject;
    public GameObject lastLedgeGrabbedObject;

	//Action en cours du perso
	public enum CharacterState
	{
		Idle,
        Braking,
		Running,
		Jumping,
		Falling,
        Projected,
        LedgeGrabbing,
        LedgeClimbing
	}
	public CharacterState charState;
    public CharacterState lastCharState;

	public int directionAnim;

    public GameObject groundCheckSphere;
    //ShereCast Information
    float shpereRadius = 0.5f;
    Vector3 sphereGroundPosition;
    public float sphereGroundDistance;


	// Use this for initialization
	public override void Start () {

        base.Start();

		//Prendre lanimator dans le sprite enfant GameObject
		//animator = GetComponentInChildren<Animator>();
		rb = this.GetComponent<Rigidbody>();
		capsule = this.GetComponent<CapsuleCollider>();

		rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;

		Vector3 lastMoveDirection = Vector3.zero;

		animator = GetComponentInChildren<Animator> ();

		//charState = CharacterState.Idle;
        ChangeCharacterState(CharacterState.Idle);

        rb.maxAngularVelocity = 0f;
        rb.interpolation = RigidbodyInterpolation.Interpolate;
	}

    public void Update()
    {
        GroundCheckSphereCast();
    }

    private void GroundCheckSphereCast(){
        //groundCheckSphere.transform.position = transform.position;
        RaycastHit hitInfoZ;
        
        Vector3 sphereCastStart = transform.position + Vector3.up * 0.6f;

        if (Physics.SphereCast(sphereCastStart, 0.5f, Vector3.down, out hitInfoZ, 100)){
            sphereGroundPosition = hitInfoZ.point; 
            sphereGroundDistance = transform.position.y - sphereGroundPosition.y;
        }
    }

    private void OnDrawGizmosSelected(){
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(sphereGroundPosition, 0.5f);
    }

    public void FixedUpdate(){
        
		SetCharacterDirectionForAnimation ();
		UpdateAnimator ();

        CheckWallStatus();
        ApplyGravity();
    }

    void ActualizeLatestDesiredDirection (Vector2 controllerDir){

        inputMoveDistance = controllerDir.magnitude;

		if (inputMoveDistance > 0f){
			inputMoveDirectionLatest.x = controllerDir.x;
			inputMoveDirectionLatest.z = controllerDir.y;

            //Normalize direction
            inputMoveDirectionLatest = inputMoveDirectionLatest / inputMoveDistance;
		}
	}

    //Methode appelee au Fixed Update par IsometricCharacterController
    public void RecieveControllerInput(Vector2 controllerDirection, bool jump, bool cancel){

        ActualizeLatestDesiredDirection(controllerDirection);

        if (isGrounded) {	
			HandleGroundedMovement (controllerDirection, jump);
		} else if(isOnLedge){
            HandleLedgedMovement(jump, cancel);
        } else {
            HandleAirborneMovement(controllerDirection);
        }    
    }

	void HandleGroundedMovement(Vector2 inputMovement, bool jump){

        if (inputMovement.magnitude > 0f)
        {
            CharacterRun();
        }
        else if (inputMovement.magnitude == 0f && rb.velocity.magnitude > 0.1f)
        {
            CharacterBrakeRunning();
        } 
        else if (inputMovement.magnitude == 0f && rb.velocity.magnitude < 0.1){
            CharacterIdle();
        }        

        if (jumpButtonReleased && jump) {
            CharacterJump();
		}
        else if (!jump)
        {
            jumpButtonReleased = true;
        }

        CheckIfStayingOnGround();
	}
    
    void HandleLedgedMovement(bool jump, bool cancel)
    {
        rb.velocity = Vector3.zero;     
        
        if (jumpButtonReleased && jump)
        {
            CharacterLedgeClimb();
        }
        else if (!jumpButtonReleased && jump)
        {

        }
        else if (!jump)
        {
            jumpButtonReleased = true;
        }

        if (cancel)
        {
            CharacterLedgeDrop();
        }
    }

	void HandleAirborneMovement(Vector2 input){

        CharacterAirMoving(input);

        //Changer letat quand le perso redescend
        if (rb.velocity.y < 0) {
			//charState = CharacterState.Falling;
            //ChangeCharacterState(CharacterState.Falling);

            CharacterFall();
		}

        CheckIfStayingInTheAir();

        //Character deviendra onLedge si necessaire
        //Check for Ledge when character is in the air
        CheckLedgeStatus();
    }

	void CharacterRun (){

        //charState = CharacterState.Running;
        ChangeCharacterState(CharacterState.Running);

        //Augmenter la vitesse d'acceleration du deplacement
        if (runningSpeedAct <= runningSpeedMax - runningSpeedAcceleration)
        {
            runningSpeedAct += runningSpeedAcceleration;
        } else {
            runningSpeedAct = runningSpeedMax;
        }

        //Acceleration dans la bonne direction
        rb.AddForce(inputMoveDirectionLatest * runningSpeedAct * Time.deltaTime, ForceMode.Acceleration);

        //Limiter la vitesse
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, velocityMagnetudeMax);
	}

	void CharacterBrakeRunning(){

        //charState = CharacterState.Braking;       
        ChangeCharacterState(CharacterState.Braking);

		//Ralentir le depacement jusqu'au freinage					
		rb.AddForce (- rb.velocity * brakingSpeed * Time.deltaTime, ForceMode.Acceleration);
	}

    void CharacterIdle(){

		//charState = CharacterState.Idle;	
        ChangeCharacterState(CharacterState.Idle);	

		runningSpeedAct = 0f;
        rb.velocity = Vector3.zero;
    }

    void CharacterJump()
    {
        //charState = CharacterState.Jumping;
        ChangeCharacterState(CharacterState.Jumping);

        rb.velocity = new Vector3(rb.velocity.x, jumpPower, rb.velocity.z);
        SetNotOnGround();

        //Wait For Jump Button to be Released to be able to Jump again
        jumpButtonReleased = false;
    }

    void CharacterFall(){

        ChangeCharacterState(CharacterState.Falling);

        SetNotOnGround();
    }

    void CharacterLanding(Vector3 landingPosition){

        SetOnGround(landingPosition);        
        
        CharacterIdle();
    }

    void CharacterProjected(){

        ChangeCharacterState(CharacterState.Projected);

        SetNotOnGround();
    }

    void CharacterAirMoving(Vector2 input){
        if(input.magnitude > 0f)
        {
            //Si on est pas a la vitesse maximum				
            Vector2 horizontalMovement = new Vector2(rb.velocity.x, rb.velocity.z);
            if(horizontalMovement.magnitude <= airMoveVelocityMagnetudeMax)
            {
                rb.AddForce(inputMoveDirectionLatest * 400 * Time.deltaTime, ForceMode.Acceleration);
            }
        }
    }

    void CharacterLedgeGrab(Vector3 hitPoint)
    {
        //Happens only at grabbing moment
        if (!isOnLedge)
        {
            SetOnLedge();

            //transform.Translate(Vector3.down * (hitPoint.y - 2));
            transform.position = new Vector3(transform.position.x, hitPoint.y - 2, transform.position.z);

            ledgeGrabPosition = hitPoint;

            ledgeGrabDestination = hitPoint - transform.position;
            ledgeGrabDestination.y = 0f;

            ledgeGrabDestination += hitPoint;


            //Send a Grab Direction that won't change to the Animation
            Vector3 animGrabDirection = Quaternion.AngleAxis(45, Vector3.up) * inputMoveDirectionLatest;
            animator.SetFloat("AnimGrabDirectionX", animGrabDirection.x);
            animator.SetFloat("AnimGrabDirectionY", animGrabDirection.z);

            //Trigger the animation only once
            //charState = CharacterState.LedgeGrabbing;
            ChangeCharacterState(CharacterState.LedgeGrabbing);

            //Wait For Jump Button to be Released to be able to Jump From Ledge
            jumpButtonReleased = false;
            canLedgeGrab = false;
        }        
    }

    void CharacterLedgeClimb()
    {

        //Vector3 ledgeDir = ledgeGrabPosition - transform.position;
        //rb.velocity = new Vector3(0f, ledgeClimbingSpeed, 0f);
        StartCoroutine(ClimbIntoLedgePosition());

        isOnLedge = false;

        //charState = CharacterState.LedgeClimbing; //A remplacer quand on aura des animations
        ChangeCharacterState(CharacterState.LedgeClimbing);
    }

    IEnumerator ClimbIntoLedgePosition(){

        while (transform.position.y <= ledgeGrabPosition.y)
        {
            rb.velocity = new Vector3(0f, ledgeClimbingSpeed, 0f);
            yield return new WaitForEndOfFrame();
        }

        while (Vector3.Distance(transform.position, ledgeGrabDestination) > 0.1)
        {
            Vector3 heading = ledgeGrabDestination - transform.position;
            float distance = heading.magnitude;
            Vector3 dir = heading/distance;

            rb.velocity = dir * ledgeClimbingSpeed / 2 + Vector3.up * 2;

            yield return new WaitForEndOfFrame();
        }
        
        //Debug.Log("Fini");

        yield return null;
    }

    void CharacterLedgeDrop()
    {
        SetNotOnLedge();
    }
   
    void CheckWallStatus(){

		RaycastHit hitInfo;

		#if UNITY_EDITOR
		Debug.DrawRay(transform.position + (Vector3.up * 0.25f), (inputMoveDirectionLatest * wallCheckDistance) + (Vector3.up * 0.25f), Color.black);
		#endif	

		if (Physics.Raycast (transform.position, inputMoveDirectionLatest, out hitInfo, wallCheckDistance)) {
			wallNormal = hitInfo.normal;
			#if UNITY_EDITOR
			Debug.DrawRay (hitInfo.transform.position, wallNormal * 10f, Color.red);
			#endif	
			isOnWall = true;
			wallDecalage = inputMoveDirectionLatest + wallNormal;

		} else {
			isOnWall = false;
			wallDecalage = Vector3.zero;
		}

		#if UNITY_EDITOR
		Debug.DrawRay (transform.position, wallDecalage * 10f, Color.red);
		#endif	
	}

    void CheckIfStayingOnGround(){
        //Checker avec un raycast, juste au dessus du bas du perso
        Vector3 checkPosition = transform.position + Vector3.up * 0.1f;

        if (sphereGroundDistance > 0.2)
        {
            if(rb.velocity.y <= 0){
                CharacterFall();
            } else {
                CharacterProjected();
            }
        }
    }

    void CheckIfStayingInTheAir()
    {
        if (!isGrounded)
        {
        
            if(rb.velocity.y < 0){

                //Voir si le personnage atterit au sol. 
                //Si la prochaine position est dans le sol, trouver la hauteur du sol pour snapper
                float predictedY = (- rb.velocity.y * Time.deltaTime);

                //Voir si on peut supprimer ce truc
                groundCheckDistance = predictedY;

                //Snapper
                if (sphereGroundDistance != 0 && sphereGroundDistance < predictedY){
                    CharacterLanding(sphereGroundPosition);
                }
            }  
        } 
    }

    void CheckLedgeStatus()
    {
        RaycastHit hitInfo;        
        Vector3 ledgeDetectStartPosition = transform.position + inputMoveDirectionLatest + Vector3.up * 2 ;

        Debug.DrawRay(ledgeDetectStartPosition,(Vector3.down), Color.red);

        //Start ledge detection from characters height
        if (canLedgeGrab && !Physics.Raycast(ledgeDetectStartPosition, Vector3.down, out hitInfo, 0.1f))
        {
            //If there is nothing higher than character
            //Look down for a ledge
            //Il n'y a pas de mur au dessus, on peut regarder vers le bas
            if (Physics.Raycast(ledgeDetectStartPosition, Vector3.down, out hitInfo, ledgeCheckDistance) && hitInfo.collider.gameObject.tag == "Ledgeable")
            {
                //Cannot Ledge again at same place
                //Last LedgeGrabbed Object is Reseted From "SetOnGround"
                if (hitInfo.normal == Vector3.up)
                {
                    CharacterLedgeGrab(hitInfo.point);
                }
                else if (hitInfo.normal.y > -0.75f && hitInfo.normal.y < 0.25f)
                {
                    Debug.Log("Ledge Normal != 1 : " + hitInfo.normal.y);
                    CharacterLedgeGrab(hitInfo.point);
                }

                lastLedgeGrabbedObject = hitInfo.collider.gameObject;          

            }
            else
            {
                //There is no ledge to climb
            }
        } 
        else
        {
            //Debug.Log("Wall Blocking a Ledge");
            SetNotOnLedge();
        }
    }

    void ANCIENCheckLedgeStatus()
    {
        RaycastHit hitInfo;        
        Vector3 ledgeDetectStartPosition = transform.position + inputMoveDirectionLatest + Vector3.up * 2 ;

        Debug.DrawRay(ledgeDetectStartPosition,(Vector3.down), Color.red);

        //Start ledge detection from characters height
        if (!Physics.Raycast(ledgeDetectStartPosition, Vector3.down, out hitInfo, 0.1f))
        {
            //If there is nothing higher than character
            //Look down for a ledge
            //Il n'y a pas de mur au dessus, on peut regarder vers le bas
            if (Physics.Raycast(ledgeDetectStartPosition, Vector3.down, out hitInfo, ledgeCheckDistance))
            {
                //Cannot Ledge again at same place
                //Last LedgeGrabbed Object is Reseted From "SetOnGround"
                if (hitInfo.collider.gameObject != lastLedgeGrabbedObject)
                {
                    Debug.Log("Different Ledge");

                    //Il y a un mur en bas, on peut s'accrocher                
                    if (hitInfo.normal == Vector3.up)
                    {
                        CharacterLedgeGrab(hitInfo.point);
                    }
                    else if (hitInfo.normal.y > -0.75f && hitInfo.normal.y < 0.25f)
                    {
                        Debug.Log("Ledge Normal != 1 : " + hitInfo.normal.y);
                        CharacterLedgeGrab(hitInfo.point);
                    }

                    lastLedgeGrabbedObject = hitInfo.collider.gameObject;
                }
                else
                {
                    Debug.Log("Cannot Ledge because of Last LedgeGrabbed Object");
                }
            }
            else
            {
                //There is no ledge to climb
            }
        } 
        else
        {
            Debug.Log("Wall Blocking a Ledge");
            SetNotOnLedge();
        }
    }

    void SetOnLedge()
    {
        if (!isOnLedge)
        {
            isOnLedge = true;
        }
    }

    void SetNotOnLedge()
    {
        if (isOnLedge)
        {
            isOnLedge = false;
        }
    }

    void SetOnGround(Vector3 hitPoint)
    {
        if (!isGrounded)
        {

            //Snap to ground
            transform.position = hitPoint;

            //Debug.Log("Switched to Grounded, Snapped");
            isGrounded = true;
            rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);
            rb.angularVelocity = new Vector3(rb.angularVelocity.x, 0f, rb.angularVelocity.z);
            
            isOnLedge = false;

            ResetLedgeParameters();            
        }
    }

    void SetNotOnGround()
    {
        if (isGrounded)
        {
            isGrounded = false;
        }
    }

    void ResetLedgeParameters()
    {
        canLedgeGrab = true;
        lastLedgeGrabbedObject = null;
    }

    void ApplyGravity()
    {
        if (!isGrounded && !isOnLedge)
        {
            Vector3 extraGravityForce = (Physics.gravity * gravityMultiplier) - Physics.gravity;
            rb.AddForce(extraGravityForce);
        }
    }

	void UpdateAnimator (){

        animator.SetInteger ("Direction", directionAnim);

        //Rotation du vecteur de direction
        //Exemple
        //vector = Quaternion.AngleAxis(-45, Vector3.up) * vector;
        Vector3 animDirection;
        animDirection = Quaternion.AngleAxis(45, Vector3.up) * inputMoveDirectionLatest;
        animator.SetFloat("AnimDirectionX", animDirection.x);
        animator.SetFloat("AnimDirectionY", animDirection.z);           

        //Si il y a un changement de CharState
        if (lastCharState != charState)
        {

            TriggerAnimation();         

            //lastCharState = charState;

        }
	}

    void ChangeCharacterState(CharacterState newState){

        if (newState != charState){
            charState = newState;
        }

        if (lastCharState != charState){
            TriggerAnimation();   
            lastCharState = charState;
        }
    }

    void TriggerAnimation(){

        //Debug.Log("TRIGGERING ANIM : " + charState);

        switch (charState)
        {
            case CharacterState.Idle:
                animator.SetTrigger("Idle");
                break;
            case CharacterState.Running:
                animator.SetTrigger("Run");
                break;
            case CharacterState.Braking:
                animator.SetTrigger("Brake");
                break;
            case CharacterState.Jumping:
                animator.SetTrigger("Jump");
                break;
            case CharacterState.Falling:
                animator.SetTrigger("Jump");
                break;
            case CharacterState.Projected:
                animator.SetTrigger("Jump");
                break;
            case CharacterState.LedgeGrabbing:
                animator.SetTrigger("LedgeGrab");
                break;
            case CharacterState.LedgeClimbing:
                animator.SetTrigger("Jump");
                break;
        }
    }



    // A supprimer quand toute les animation utiliserons un BLEND TREE
	void SetCharacterDirectionForAnimation(){
		
		int angle = Mathf.RoundToInt(Vector3.SignedAngle(inputMoveDirectionLatest, Vector3.forward, Vector3.up));

        angle += 45;


        if (angle > -22.5 && angle < 22.5)
        {
            directionAnim = 0; //Droite
        }
        else if (angle > 22.5 && angle < 67.5)
        {
            directionAnim = 7;
        }
        else if (angle > 67.5 && angle < 112.5)
        {
            directionAnim = 6; //Haut
        }
        else if (angle > 112.5 && angle < 157.5)
        {
            directionAnim = 5;
        }
        else if (angle > 157.5 || angle < -157.5)
        {
            directionAnim = 4; // Gauche
        }
        else if (angle > -157.5 && angle < -112.5)
        {
            directionAnim = 3;
        }
        else if (angle > -112.5 && angle < -67.5)
        {
            directionAnim = 2; //Bas
        }
        else if (angle > -67.5 && angle < -22.5)
        {
            directionAnim = 1;
        }
    }

}
