﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubesPhysics : MonoBehaviour, IExplosable {


	Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Explode(float expForce, Vector3 posExplosion){
		rb.AddForce(posExplosion * expForce);

	}
}
