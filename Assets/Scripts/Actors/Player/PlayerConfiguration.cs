﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerConfiguration
{
    public int playerNumber;
    public InControl.InputDevice playerDevice;
    public Color playerColor;    
    public CharacterCard playerCharacterCard;

    public PlayerConfiguration(int number, InControl.InputDevice device, Color color){
        this.playerNumber = number;
        this.playerDevice = device;
        this.playerColor = color;
    }

    public PlayerConfiguration(int number){
        this.playerNumber = number;
    }

    public PlayerConfiguration(){

    }


}