﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PlayerCard : ScriptableObject {

	public int playerNumber;

	public InControl.InputDevice device;

	public string playerName;

	public Color playerColor;

	public bool isConnected;

	public string usingCharacter;

	public string usingController;

}
