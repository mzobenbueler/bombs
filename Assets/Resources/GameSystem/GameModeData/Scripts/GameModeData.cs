﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModeData : ScriptableObject {

	public GameMode gameMode;

	public string gameModeName;
    public string gameModeDescription;

	public int gameModeMinPlayer;
	public int gameModeMaxPlayer;

	public Sprite gameModeSprite;
}
