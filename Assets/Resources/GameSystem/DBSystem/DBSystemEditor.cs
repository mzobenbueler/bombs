﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System;
using System.Reflection;

public class DBSystemEditor : EditorWindow
{
    Texture2D moduleSectionTexture;
    Texture2D gridSectionTexture;
    Texture2D formSectionTexture;
    Texture2D toolbarSectionTexture;
    Texture2D gridCaseTexture;

    Rect moduleSection = new Rect();
    Rect gridSection = new Rect();
    Rect formSection = new Rect();
    Rect toolbarSection = new Rect();


    Color moduleSectionColor = new Color(75f / 255f, 75f / 255f, 75f / 255f, 1f);
    Color gridSectionColor = new Color(10 / 255f, 10 / 255f, 10 / 255f, 1f);
    Color formSectionColor = new Color(100f / 255f, 100f / 255f, 100f / 255f, 1f);
    Color toolbarSectionColor = new Color(200f / 255f, 200f / 255f, 200f / 255f, 1f);
    Color gridCaseColor = new Color(240f / 255f, 240f / 255f, 240f / 255f, 1f);

    float sidenavWidth;
    float itemListHeight;
    float fieldSectionHeight;
    float toolbarHeight;

    //Grid Info
    Texture2D gridSeparatorTexture;
    Rect gridSeparator;
    Color gridSeparatorColor = new Color(255f / 255f, 255f / 255f, 255f / 255f, 1f);
    float gridSeparatorSize;

    //Selected module information
    ScriptableObject[] availableModules_SO; //Scriptable Object containing lists for every items
    string selectedModule_Name = ""; //Voir si c'est toujours utile
    FieldInfo[] selectedModule_Fields;
    List<string> selectedModule_stringFields;
    List<string> selectedModule_floatFields;
    List<string> selectedModule_intFields;
    GameObject[] selectedModule_enumFields;

    ScriptableObject selectedItem_SO;
    //SerializedProperty[] selectedItem_Properties;
    List<SerializedProperty> selectedItem_Properties;

    //Grid dimensions information
    Vector2 grid_totalSize;
    Vector2Int grid_casesNumber;
    Vector2 grid_caseSize;
    Vector2 grid_separatorSize = new Vector2(2f,2f);
    Rect[,] grid_Cases;

    //System.Collections.IList moduleList;
    System.Collections.IList moduleItemList;
    object[,] columnObjects;
    string exemple;
    int gridColumnsNo = 1;

    //Form information


    [MenuItem("Window/Max DBSystem")]
    static void OpenWindow()
    {
        DBSystemEditor window = (DBSystemEditor)GetWindow(typeof(DBSystemEditor));
        window.minSize = new Vector2(1200, 400);
        window.Show();
    }

    void OnEnable()
    {
        InitLayout();
        InitAvailableModules();
        InitGrid();
    }

    void OnGUI()
    {
        DrawLayouts();
        DrawModuleSection();
        DrawItemListSection();
        DrawFieldSection();
        DrawToolbarSection();
    }

    #region Layout : Initialization and Drawing

    void InitLayout(){
        InitDimensions();
        InitTextures();
    }

    void InitDimensions()
    {
        sidenavWidth = Screen.width / 4;
        itemListHeight = Screen.height / 2 - Screen.height / 15;
        fieldSectionHeight = itemListHeight;
        toolbarHeight = Screen.height / 15;
    }

    void InitTextures()
    {
        moduleSectionTexture = new Texture2D(1, 1);
        moduleSectionTexture.SetPixel(0, 0, moduleSectionColor);
        moduleSectionTexture.Apply();

        gridSectionTexture = new Texture2D(1, 1);
        gridSectionTexture.SetPixel(0, 0, gridSectionColor);
        gridSectionTexture.Apply();

        formSectionTexture = new Texture2D(1, 1);
        formSectionTexture.SetPixel(0, 0, formSectionColor);
        formSectionTexture.Apply();

        toolbarSectionTexture = new Texture2D(1, 1);
        toolbarSectionTexture.SetPixel(0, 0, toolbarSectionColor);
        toolbarSectionTexture.Apply();

        gridCaseTexture = new Texture2D(1, 1);
        gridCaseTexture.SetPixel(0, 0, gridCaseColor);
        gridCaseTexture.Apply();
    }

    void DrawLayouts()
    {
        moduleSection.x = 0;
        moduleSection.y = 0;
        moduleSection.width = sidenavWidth;
        moduleSection.height = Screen.height;
        GUI.DrawTexture(moduleSection, moduleSectionTexture);

        gridSection.x = sidenavWidth;
        gridSection.y = 0;
        gridSection.width = Screen.width - sidenavWidth;
        gridSection.height = itemListHeight;
        GUI.DrawTexture(gridSection, gridSectionTexture);

        formSection.x = sidenavWidth;
        formSection.y = itemListHeight + toolbarHeight;
        formSection.width = Screen.width - sidenavWidth;
        formSection.height = fieldSectionHeight;
        GUI.DrawTexture(formSection, formSectionTexture);

        toolbarSection.x = sidenavWidth;
        toolbarSection.y = itemListHeight;
        toolbarSection.width = Screen.width - sidenavWidth;
        toolbarSection.height = toolbarHeight;
        GUI.DrawTexture(toolbarSection, toolbarSectionTexture);
    }

    #endregion

    #region Module : List Initialization

    //Creer la liste des modules disponibles grace au Type ScriptableObjectsList
    void InitAvailableModules()
    {
        System.Type soListType = typeof(ScriptableObjectsList);
        FieldInfo[] soListFields = soListType.GetFields();

        availableModules_SO = new ScriptableObject[soListFields.Length];
        for (int i = 0; i < availableModules_SO.Length; i++)
        {
            availableModules_SO[i] = GetScriptableObjectFromType(soListFields[i].FieldType);
        }
    }

    ScriptableObject GetScriptableObjectFromType(Type soType)
    {
        ScriptableObject soFound;
        string[] tempGUIDs = AssetDatabase.FindAssets("t:" + soType);
        string path = AssetDatabase.GUIDToAssetPath(tempGUIDs[0]);
        soFound = AssetDatabase.LoadAssetAtPath<ScriptableObject>(path);
        return soFound;
    }

    #endregion 

    //Contient SET COLUMN VALUES - METTRE CA AILLEURS
    #region Module : Get Items, Fields and Values

    void GetModuleInformation(ScriptableObject selectedModuleSO)
    {
        selectedModule_Name = selectedModuleSO.name;
        object so_object = selectedModuleSO;

        //Type du Scriptable Object
        Type soType = selectedModuleSO.GetType();

        //Champ du Scriptable Object (Ici, une liste)
        FieldInfo[] fields = soType.GetFields();

        //Il n'a qu'un seul field qui est une liste
        Type fieldType = fields[0].FieldType;

        GetModuleFields(fieldType.GetElementType());
        GetModuleItemsAndValues(so_object, fields[0], fieldType);
    }

    void GetModuleFields(Type type)
    {
        selectedModule_Fields = type.GetFields();
    }

    void GetModuleItemsAndValues(object obj, FieldInfo field, Type fieldType)
    {
        Debug.Log("SET MODULE ITEM LIST : OBJ : " + obj + ", FIELD : " + field + ", FIELD TYPE : " + fieldType);
        if (fieldType.IsArray)
        {
            moduleItemList = (System.Collections.IList)field.GetValue(obj);
        }
    }    

    void GetModuleItemsAndValuesANCIENT(object obj, FieldInfo field, Type fieldType)
    {
        Debug.Log("SET MODULE ITEM LIST : OBJ : " + obj + ", FIELD : " + field + ", FIELD TYPE : " + fieldType);
        if (fieldType.IsArray)
        {
            moduleItemList = (System.Collections.IList)field.GetValue(obj);

            Debug.Log(moduleItemList[0].ToString());
            
            string[] scriptableObjectName = moduleItemList[0].ToString().Split(' ');

            //GetSelectedItem(scriptableObjectName[0]);

            //Voir apres, pour modifier des valeurs
            //FieldInfo myFIeld = moduleItemList[0].GetType();

            //I hope this the FieldInfo that you want to get:
            //FieldInfo myValueFieldInfo = sampleObject_test1_Element0.GetType().GetField("MyValue");

            //Now it is possible to read and write values
            //object sampleObject_test1_Element0_MyValue = myValueFieldInfo.GetValue(sampleObject_test1_Element0);
        }
    }



    #endregion

    #region User Interaction

    void User_SelectModule(ScriptableObject selectedMod){

        GetModuleInformation(selectedMod);

        SetGridParameters(new Vector2Int(selectedModule_Fields.Length,10));

        InitColumnValues();

        SelectFirstItemByDefault();
    }

    void SelectFirstItemByDefault(){
        GetSelectedItem(0);
    }

    void User_SelectItem(){

    }

    #endregion
 
    #region Draw Module Section

    void DrawModuleSection()
    {
        GUILayout.BeginArea(moduleSection);
        GUILayout.Label("Modules");

        for (int i = 0; i < availableModules_SO.Length; i++)
        {
            //GUILayout.Label (sectionNames[i].Name.ToString());
            if (GUILayout.Button(availableModules_SO[i].name))
            {
                //Debug.Log("Clicked Button : " + availableModules_SO[i].name);
                User_SelectModule(availableModules_SO[i]);
            }
        }
        GUILayout.EndArea();
    }

    #endregion

    #region Grid : Init and Draw

    void InitGrid(){
        SetGridParameters(new Vector2Int(10,10));
    }

    void SetGridParameters(Vector2Int case_Number){

        grid_casesNumber = case_Number;
        grid_caseSize.x = (gridSection.width / grid_casesNumber.x) - grid_separatorSize.x;
        grid_caseSize.y = (gridSection.height / grid_casesNumber.y) - grid_separatorSize.y;
        grid_Cases = new Rect[grid_casesNumber.x, grid_casesNumber.y];

        for (int i = 0; i < grid_casesNumber.x; i++)
        {
            for (int j = 0; j < grid_casesNumber.y; j++)
            {
                grid_Cases[i, j].x = (gridSection.width / grid_casesNumber.x) * i;
                grid_Cases[i, j].y = (gridSection.height / grid_casesNumber.y) * j;
                grid_Cases[i, j].width = grid_caseSize.x;
                grid_Cases[i, j].height = grid_caseSize.y;
            }
        }   
    }

    void InitColumnValues()
    {
        columnObjects = new object[selectedModule_Fields.Length, moduleItemList.Count];
        for (int i = 0; i < selectedModule_Fields.Length; i++)
        {
            columnObjects[i, 0] = moduleItemList[0];
            for (int j = 0; j < moduleItemList.Count; j++)
            {
                FieldInfo itemValue = moduleItemList[j].GetType().GetField(selectedModule_Fields[i].Name);
                columnObjects[i, j] = itemValue.GetValue(moduleItemList[j]);
            }
        }
    }
    
    void DrawGridCases(){
        foreach(Rect caseRect in grid_Cases){
            GUI.DrawTexture(caseRect, gridCaseTexture);        
        }
    }

    void DrawItemListSection()
    {
        GUILayout.BeginArea(gridSection);
        GUILayout.Label("Item Grid");
        DrawGridCases();
        if (columnObjects != null){
            DrawGridValues();
        }
        GUILayout.EndArea();
    }

    void DrawGridValues(){
        for(int i = 0; i < grid_casesNumber.x; i ++){
            for(int j = 0; j < moduleItemList.Count; j ++){
                GUILayout.BeginArea(grid_Cases[i, j]);
                if (columnObjects[i, j] != null)
                {
                    GUILayout.Label(columnObjects[i, j].ToString());
                    GUILayout.Label(columnObjects[i, j].GetType().ToString());
                }
                GUILayout.EndArea();
            }
        }
    } 

    #endregion

    #region Form

    void InitForm(){

    }

    void DrawFieldSection()
    {
        GUILayout.BeginArea(formSection);
        GUILayout.Label("Form");

        //DrawModuleFields();

        DrawSelectedItemForm();

        GUILayout.EndArea();
    }

    void DrawSelectedItemForm(){

        if(selectedItem_Properties != null){
            foreach(SerializedProperty prop in selectedItem_Properties){
                EditorGUILayout.PropertyField(prop, new GUIContent(prop.name));
            }
        }
    }

    void GetSelectedItem(int itemNo)
    {
        Debug.Log(moduleItemList[itemNo].ToString());
        
        string[] scriptableObjectName = moduleItemList[itemNo].ToString().Split(' ');

        //GetSelectedItem(scriptableObjectName[0]);

        Debug.Log("Looking for : " + scriptableObjectName[itemNo]);
        //ScriptableObject soFound;

        string[] tempGUIDs = AssetDatabase.FindAssets(scriptableObjectName[itemNo]);

        if (tempGUIDs.Length < 1)
        {
            Debug.Log("Aucun trouve");
            return;
        }

        string path = AssetDatabase.GUIDToAssetPath(tempGUIDs[0]);
        selectedItem_SO = AssetDatabase.LoadAssetAtPath<ScriptableObject>(path);
        Debug.Log("Scriptable Object Found By Name : " + selectedItem_SO.name);

        if(selectedItem_SO != null){

            selectedItem_Properties = new List<SerializedProperty>();

            SerializedObject so = new SerializedObject(selectedItem_SO);            
            
            Type selectedSO_Type = selectedItem_SO.GetType();

            FieldInfo[] selectedSO_Fields = selectedSO_Type.GetFields();

            foreach(FieldInfo fieldZ in selectedSO_Fields){
                selectedItem_Properties.Add(so.FindProperty(fieldZ.Name));
            }
        }
    }

    void DrawModuleFields()
    {
        if (selectedModule_Fields != null)
        {




                // //I hope this the FieldInfo that you want to get:
                // FieldInfo myValueFieldInfo = sampleObject_test1_Element0.GetType().GetField("MyValue");

                // //Now it is possible to read and write values
                // object sampleObject_test1_Element0_MyValue = myValueFieldInfo.GetValue(sampleObject_test1_Element0);

                // Console.WriteLine(sampleObject_test1_Element0_MyValue); // prints 99
                // myValueFieldInfo.SetValue(sampleObject_test1_Element0, 55);
                // sampleObject_test1_Element0_MyValue = myValueFieldInfo.GetValue(sampleObject_test1_Element0);
                // Console.WriteLine(sampleObject_test1_Element0_MyValue); // prints 55


            //object laValeur = selectedModule_Fields[0].GetValue(selectedItem);

            //Debug.Log("LA VALEUR : " + laValeur);

            //selectedModule_Fields[0].SetValue(laValeur, " AH BON OUI");

            //Debug.Log("LA VALEUR : " + laValeur);

            int item = 0;
            int field = 0;

            //selectedModule_Fields[field].SetValue(columnObjects[field, item], " OH SHIET");

            //Debug.Log(columnObjects[field, item]);

            //columnObjects[field, item] = "ZZZ";


            int fieldsCount = selectedModule_stringFields.Count + selectedModule_intFields.Count + selectedModule_floatFields.Count;

            for (int i = 0; i < selectedModule_Fields.Length; i++)
            {
                //GUILayout.Label(selectedModuleFields[i].Name);            
                //exemple = GUILayout.TextField(selectedModuleFields[i].Name, "textfield");
            }

            foreach(string str in selectedModule_stringFields){
                DrawStringField(str);
            }
        }
    }

    void DrawStringField(string str){        
        str = EditorGUILayout.TextField(str, "textfield ");
    }

    void DrawIntField(string str){
        int zzz = 0;
        zzz = EditorGUILayout.IntField(str, zzz);
    }

    void DrawFloatField(string str){
        float abc = 0.1f;
        abc = EditorGUILayout.FloatField(str, abc);
    }

    void DrawEnumField(){

    }

    #endregion

    #region ToolBar

    void DrawToolbarSection()
    {
        GUILayout.BeginArea(toolbarSection);
        GUILayout.BeginHorizontal();
        GUILayout.Label("Module : " + selectedModule_Name);
        //EditorGUI.InspectorTitlebar(toolbarSection, false, selectedModule, false);
        DrawToolbarButtons();
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    void DrawToolbarButtons()
    {
        if (GUILayout.Button("NEW"))
        {
            Debug.Log("Clicked Button : NEW ");
        }
        if (GUILayout.Button("SAVE"))
        {
            Debug.Log("Clicked Button : SAVE ");
        }
        if (GUILayout.Button("DELETE"))
        {
            Debug.Log("Clicked Button : DELETE ");
        }
    }

    #endregion

    #region Poubelle ?

    void DrawFullGridANCIEN()
    {
        if (columnObjects != null)
        {
            int xCases = selectedModule_Fields.Length;
            int yCases = moduleItemList.Count;

            float xSize = (gridSection.width / xCases);
            float ySize = (gridSection.height / yCases);

            grid_Cases = new Rect[xCases, yCases];

            for (int i = 0; i < xCases; i++)
            {
                for (int j = 0; j < yCases; j++)
                {
                    grid_Cases[i, j].x = (gridSection.width / xCases) * i;
                    grid_Cases[i, j].y = (gridSection.height / yCases) * j;

                    grid_Cases[i, j].width = xSize - 2f;
                    grid_Cases[i, j].height = ySize - 2f;

                    GUI.DrawTexture(grid_Cases[i, j], gridCaseTexture);

                    GUILayout.BeginArea(grid_Cases[i, j]);

                    if (columnObjects[j, i] != null)
                    {
                        GUILayout.Label(columnObjects[j, i].ToString());
                        GUILayout.Label(columnObjects[j, i].GetType().ToString());

                    }
                    else
                    {
                        //Debug.Log("VIDE : " + i + "; " + j);  
                    }
                    GUILayout.EndArea();
                }
            }
        }
    }

    void DrawFullGridANCIENANCIEN()
    {
        if (columnObjects != null)
        {
            int xCases = selectedModule_Fields.Length;
            int yCases = moduleItemList.Count;

            float xSize = (gridSection.width / xCases);
            float ySize = (gridSection.height / yCases);

            Debug.Log(columnObjects.Length);
            Debug.Log(xCases + " O " + yCases);

            grid_Cases = new Rect[xCases, yCases];

            for (int i = 0; i < yCases; i++)
            {
                for (int j = 0; j < xCases; j++)
                {
                    grid_Cases[i, j].x = (gridSection.width / xCases) * i;
                    grid_Cases[i, j].y = (gridSection.height / yCases) * j;

                    grid_Cases[i, j].width = xSize - 2f;
                    grid_Cases[i, j].height = ySize - 2f;

                    GUI.DrawTexture(grid_Cases[i, j], gridCaseTexture);

                    GUILayout.BeginArea(grid_Cases[i, j]);

                    Debug.Log(i + " x " + j);


                    if (columnObjects[i, j] != null)
                    {
                        string value = columnObjects[i, j].ToString();
                        Debug.Log(value);
                    }
                    else
                    {
                        Debug.Log("VIDE : " + i + "; " + j);
                    }



                    //GUILayout.Label(columnValues[i,j].ToString());  

                    GUILayout.EndArea();
                }
            }
        }
    }

    void DrawColumns(int columns)
    {
        for (int i = 0; i < columns; i++)
        {
            Rect columnsRect = new Rect(i * gridSection.width / columns, 0f, gridSection.width / columns - 4f, gridSection.height);
            GUI.DrawTexture(columnsRect, moduleSectionTexture);

            GUILayout.BeginArea(columnsRect);
            GUILayout.Label(selectedModule_Fields[i].Name);
            GUILayout.EndArea();
        }
    }

    void DrawVerticalSeparator(int columns)
    {
        for (int i = 1; i < columns; i++)
        {
            Rect verticalSeparator = new Rect(i * gridSection.width / columns, 0f, 2f, gridSection.height);
            GUI.DrawTexture(verticalSeparator, moduleSectionTexture);

            GUILayout.BeginArea(verticalSeparator);
            GUILayout.Label(selectedModule_Fields[i].Name);
            GUILayout.EndArea();
        }
    }

    void DrawGridAncien()
    {
        if (moduleItemList != null)
        {
            for (int i = 0; i < moduleItemList.Count; i++)
            {
                GUILayout.Label(moduleItemList[i].ToString());
            }
        }
    }

    void InitColumnValuesANCIEN()
    {
        columnObjects = new object[moduleItemList.Count, selectedModule_Fields.Length];

        for (int i = 0; i < moduleItemList.Count; i++)
        {
            columnObjects[i, 0] = moduleItemList[0];
            for (int j = 0; j < selectedModule_Fields.Length; j++)
            {
                FieldInfo itemValue = moduleItemList[i].GetType().GetField(selectedModule_Fields[j].Name);
                columnObjects[i, j] = itemValue.GetValue(moduleItemList[i]);
                
                Debug.Log(i + "; " + j);
            }
        }
    }

    void ASUPPRIMERDrawGrid()
    {
        if (selectedModule_Fields != null)
        {
            //CECI DOIT SE FAIRE UNIQUEMENT AU MOMENT OU ON CHOISIT UN ITEM
            //PAS BESOIN DE LE FAIRE TOUT LE TEMPS LA
            //DrawVerticalSeparator(selectedModuleFields.Length);
            //DrawColumns(selectedModuleFields.Length);
        }

        if (moduleItemList != null)
        {
            for (int i = 0; i < moduleItemList.Count; i++)
            {
                GUILayout.Label(moduleItemList[i].ToString());
            }
        }
    }

    void INUTILEPOURLINSTANTSetItemsValues()
    {
        FieldInfo itemValue = moduleItemList[0].GetType().GetField("description");
        object lobject = itemValue.GetValue(moduleItemList[0]);

        Debug.Log(lobject);

        foreach (FieldInfo field in selectedModule_Fields)
        {
            FieldInfo itemValueTest = moduleItemList[0].GetType().GetField(field.Name);
            object lobjectz = itemValueTest.GetValue(moduleItemList[0]);
            Debug.Log(field.Name + " === " + lobjectz);            
        }
    }

    void GetValuesFromScriptableObjectANCIEN(ScriptableObject dataObject)
    {

        Type soType = dataObject.GetType();
        Debug.Log("Selected Scriptable Object : " + soType);

        //CECI PROVIENT DU SOTYPE
        // FieldInfo[] fields = soType.GetFields();
        // foreach(FieldInfo f in fields){
        //     //Debug.Log(f);
        //     //Debug.Log("Champs du Scriptable Object : " + f.GetType() + " : " + f.DeclaringType);

        // }

        object sampleObject = dataObject;

        FieldInfo[] scriptableObjectFields = soType.GetFields();

        foreach (FieldInfo soField in scriptableObjectFields)
        {
            Debug.Log("Champ du Scriptable Object : " + soField.FieldType);
            Debug.Log("Champ du Scriptable Object : " + soField.FieldType);

            var fieldType = soField.FieldType;
            Debug.Log(fieldType.IsArray);
            Debug.Log(fieldType.ReflectedType); //NULL
            Debug.Log(fieldType.Attributes);
            Debug.Log(fieldType.DeclaringType); //NULL
            Debug.Log(fieldType.GetElementType());

            GetModuleFields(fieldType.GetElementType());
            //Debug.Log(zzz.ReflectedType);
            //Debug.Log(zzz.IsArray);
            //Debug.Log(zzz.ReflectedType);

            if (fieldType.IsArray)
            {
                // We can cast to ILIst because arrays implement it and we verfied that it is an array in the if statement
                System.Collections.IList sampleObject_test1 = (System.Collections.IList)soField.GetValue(sampleObject);

                //moduleList = sampleObject_test1;

                // We can now get the first element of the array of Test2s:
                object sampleObject_test1_Element0 = sampleObject_test1[0];

                foreach (object abcde in sampleObject_test1)
                {
                    Debug.Log(abcde);
                }

                Debug.Log(sampleObject_test1_Element0.ToString());


                //I hope this the FieldInfo that you want to get:
                FieldInfo myValueFieldInfo = sampleObject_test1_Element0.GetType().GetField("MyValue");

                //Now it is possible to read and write values
                object sampleObject_test1_Element0_MyValue = myValueFieldInfo.GetValue(sampleObject_test1_Element0);

                Console.WriteLine(sampleObject_test1_Element0_MyValue); // prints 99
                myValueFieldInfo.SetValue(sampleObject_test1_Element0, 55);
                sampleObject_test1_Element0_MyValue = myValueFieldInfo.GetValue(sampleObject_test1_Element0);
                Console.WriteLine(sampleObject_test1_Element0_MyValue); // prints 55
            }
        }
    }

    void POURPLUSTARDSETVALUEBLABLA()
    {
        // // We can cast to ILIst because arrays implement it and we verfied that it is an array in the if statement
        // System.Collections.IList sampleObject_test1 = (System.Collections.IList)f.GetValue(sampleObject);
        // // We can now get the first element of the array of Test2s:
        // object sampleObject_test1_Element0 = sampleObject_test1[0];

        // Debug.Log(sampleObject_test1_Element0.ToString());

        // // I hope this the FieldInfo that you want to get:
        // //FieldInfo myValueFieldInfo = sampleObject_test1_Element0.GetType().GetField("MyValue");

        // // Now it is possible to read and write values
        // object sampleObject_test1_Element0_MyValue = myValueFieldInfo.GetValue(sampleObject_test1_Element0);

        // Console.WriteLine(sampleObject_test1_Element0_MyValue); // prints 99
        // myValueFieldInfo.SetValue(sampleObject_test1_Element0, 55);
        // sampleObject_test1_Element0_MyValue = myValueFieldInfo.GetValue(sampleObject_test1_Element0);
        // Console.WriteLine(sampleObject_test1_Element0_MyValue); // prints 55
    }

    #endregion

}