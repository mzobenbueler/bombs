﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New ScriptableObject List", menuName = "Max ScriptableObjects/New SO List")]
public class ScriptableObjectsList : ScriptableObject
{

    public BombCardList bombCardsList;

    public CharacterCardList characterCardsList;

    public CharacterColors characterColorsList;

    public GameModeListData gameModesList;

    public LevelListData levelsList;

    //Pour plus tard, faire un vrai SO PlayerCardList et le mettre ici
    public PlayerCardList playerCardsList;

}
